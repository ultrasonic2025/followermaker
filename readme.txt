Followermaker, created by Superstarxalien based on Kartmaker

HOW TO USE:
- Copy one of the example/ folders and rename it (your WAD will be named after this folder)
- Edit values in "properties.txt" to set various properties for your follower, and adjust the RGB colors that the
  converter considers transparent pixels if you want
  - Each editable field in properties.txt comes with additional information on its use
- Create or paste your follower's sprites into the template provided
- Replace the OGG sound file in the folder with the sound that you want your follower to have (this program does not convert
  or add echo for you)
- Drag the folder onto followermaker.exe, or run followermaker.exe and pass the folder as an argument
- The WAD will be placed next to your follower's source folder, ready to play

The main goal of Followermaker is to allow for base-level creation of followers. It is not a comprehensive program, and as
such you may still need to use SLADE in order to make full use of the features that follower creation provides.

Ignore build.bat and the src/ folder for standard use. This is a compile script and source files. You can use it if you
want to modify the program (you need GCC installed). I make no guarantees that the program compiles on other setups.

Huge thanks to casual koopa for creating the advanced template images as well as the instructions for those, and tips on
general aspects of the program.

Licensed under the GPL.